<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_8   25                                _a611a4</name>
   <tag></tag>
   <elementGuidId>2abc2564-205c-40e1-ae3b-6662e0c50e82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#form-change-pass</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-change-pass']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>form-change-pass</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://myid.buu.ac.th/ad/changepwd</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                
                
                    Change Password
                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-change-pass&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@id='form-change-pass']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div/form</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เปลี่ยนรหัสผ่าน (Change Password)'])[1]/following::form[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[@id = 'form-change-pass' and (text() = '
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                
                
                    Change Password
                
            ' or . = '
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                
                
                    Change Password
                
            ')]</value>
   </webElementXpaths>
</WebElementEntity>
