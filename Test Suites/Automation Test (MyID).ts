<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Automation Test (MyID)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>8f229dc8-4b75-4391-9c60-c19af9f0ba13</testSuiteGuid>
   <testCaseLink>
      <guid>5d844b26-6aa8-4d1b-8106-11fce6ca9616</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password with a length less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05dba8ed-c103-4e68-94d2-5e9cb5825ac9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without letters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef3fdf3f-b5db-43f9-af57-d1f78388dd55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without numbers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0ac6c2b-147f-4d3f-a718-35fc35324d19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without special characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f000408-b03e-4b14-81bd-1bc2486db098</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b58d620-3c5a-4008-8a94-1d7a3bd3bdc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Successful</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c08648d9-77ac-41e4-9ad6-98fca6de1635</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout Successful</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2098eff4-a4ba-4e86-b7b3-c1129bd58527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password change successfully</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
